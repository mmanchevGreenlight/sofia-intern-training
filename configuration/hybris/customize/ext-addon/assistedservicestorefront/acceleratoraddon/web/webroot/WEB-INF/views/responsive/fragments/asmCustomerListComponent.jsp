<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:set var="baseUrl" value="/assisted-service-querying/listCustomers"/>
<spring:url value="${baseUrl}" var="customerListUrl" htmlEscape="false"><spring:param name="customerListUId" value=""/></spring:url>

<div class="customer-list-select-wrapper">
    <select id="sortOptions${top ? '1' : '2'}" name="sort" class="customer-list-select js-customer-list-select" data-search-url="${customerListUrl}">
        <c:forEach items="${availableLists}" var="availableCustomerLists">
            <option value="${availableCustomerLists.uid}"><c:out value="${availableCustomerLists.name}"/></option>
        </c:forEach>
    </select>
</div>