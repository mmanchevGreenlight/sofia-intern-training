Gradle tasks

Task - Description - Dependencies
installGitHooks - Installs the git configuration to ensure all commits follow the same patter  - 
downloadHybris - Downloads from S3 the require version for this installation - Need to provide property version with the version to downlowad e.g. 5.7.0.11
installHybrisExtensions - Unzips only the required folders from the hybris distribution binaries - Need to provide property version with the version to downlowad e.g. 5.7.0.11 . - Depends on the downloadHybris task or an exisiting distribution zip file must exist
installRecipe - Setups hybris using the specified custom recipe - Need to provide property recipe with the recipe name to install e.g. b2c_ipsg_hop_responsive - Depends on the installHybrisExtensions task or a previous setup of the hybris platform
setupEnvironment - Executes tasks: installGitHooks installHybrisExtensionsinstallRecipe in the correct order to setup an environment - An existing hybris distribution file or manually invoke the downloadHybris task