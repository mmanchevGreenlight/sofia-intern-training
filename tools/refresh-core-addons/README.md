# Refresh Core Addons

## Developer Instructions

### 1. Install requirements if not already on system:
1. Node
2. NPM
3. Gulp – https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md

### 2. Setup
1. Go to the folder: {code}cd tools/refresh-core-addons{code}.
2. Install dependencies (you may need to `sudo`): {code}npm install{code}.

### 3a. Run Script Only
1. Run the task {code}gulp{code}.
2. This will:
2.1. Copy files from addons – `euronicsb2baddon, euronicsb2caddon, euronicscommonaddon` – `/acceleratoraddon/web/webroot` to the corresponding storefronts.
2.2. Copy make addon files from `euronicscommonaddon/acceleratoraddon/web/webroot` to the root.
2.3. Compile the storefront `_ui` Less and JS.

### 3b. Run Script & Watch
1. Run the task {code}gulp watch{code}
2. Listen for changes to the files and copy them over on change.

