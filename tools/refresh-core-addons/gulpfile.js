
// Includes
var _           = require('lodash');
var gulp        = require('gulp');
var cache       = require('gulp-cached');
var less        = require('gulp-less');
var rename      = require('gulp-rename');
// var browserSync = require('browser-sync');


// Config - Base
var config = {

	log: false, 

	rootDir: '../../hybris/bin/custom',

	stores: {
		b2b: {
			id: 'membersstorefront',
			addons: [
				'euronicscommonaddon',
				'euronicsb2baddon'
			]
		},
		b2c: {
			id: 'euronicsstorefront',
			addons: [
				'euronicscommonaddon',
				'euronicsb2caddon'
			]
		}
	}

};


// Util – Log Path

function logPath(path, prefix) {
	if (config.log) {
		console.log((prefix ? prefix + ': ' : '') + path.dirname + '/' + path.basename + path.extname)
	}
}


// Task - Addons Build
function addons(store) {

	var
		files   = _.map(store.addons, function(addon) {
			return  '$addon/acceleratoraddon/web/webroot/**/*.*'.replace('$addon', addon)
		}),
		find    = '$addon/acceleratoraddon/web/webroot/(_ui|WEB-INF/(.*?))/'.replace('$addon', '(' + store.addons.join('|') + ')'),
		replace = '$store/web/webroot/$2/addons/$1/'.replace('$store', store.id);

	return gulp.src(files, {cwd: config.rootDir, base: config.rootDir})
		
		// Cache
		.pipe(cache('addonbuild' + store.id))

		// To Store Addons
		.pipe(rename(function(path) {
			logPath(path, 'i');
			path.dirname = path.dirname.replace(new RegExp(find), replace);
			logPath(path, 'o');
		}))
		.pipe(gulp.dest(config.rootDir));

}
gulp.task('addonsb2b', function() { return addons(config.stores.b2b); });
gulp.task('addonsb2c', function() { return addons(config.stores.b2c); });
gulp.task('addons', ['addonsb2b', 'addonsb2c']);


// Task – JS Build
function jsBuild(store) {

	var
		files   = '$store/web/webroot/WEB-INF/_ui-src/addons/euronicscommonaddon/**/*.js'.replace('$store', store.id),
		find    = '/_ui-src/addons/euronicscommonaddon/',
		replace = '/_ui-src/';

	return gulp.src(files, {cwd: config.rootDir, base: config.rootDir})
		.pipe(cache('jsbuild' + store.id))
		.pipe(rename(function(path) {
			logPath(path, 'jsb i');
			path.dirname = path.dirname.replace(find, replace);
			logPath(path, 'jsb o');
		}))
		.pipe(gulp.dest(config.rootDir))

}
gulp.task('jsbuildb2b', ['addonsb2b'], function() { return jsBuild(config.stores.b2b); });
gulp.task('jsbuildb2c', ['addonsb2c'], function() { return jsBuild(config.stores.b2c); });
gulp.task('jsbuild', ['jsbuildb2b', 'jsbuildb2c']);


// Task – JS Dist
function jsDist(store) {

	var
		files   = '$store/web/webroot/WEB-INF/_ui-src/responsive/common/**/*.js'.replace('$store', store.id),
		find    = /\/WEB-INF\/_ui-src\/responsive\/common\/js\/[^\/]*/,
		replace = '/_ui/responsive/common/js';

	return gulp.src(files, {cwd: config.rootDir, base: config.rootDir})
		.pipe(cache('jsdist' + store.id))
		.pipe(rename(function(path) {
			logPath(path, 'jsd i');
			path.dirname = path.dirname.replace(find, replace);
			logPath(path, 'jsd o');
		}))
		.pipe(gulp.dest(config.rootDir));

}
gulp.task('jsdistb2b', ['jsbuildb2b'], function() { return jsDist(config.stores.b2b); });
gulp.task('jsdistb2c', ['jsbuildb2c'], function() { return jsDist(config.stores.b2c); });
gulp.task('jsdist', ['jsdistb2b', 'jsdistb2c']);


// Task – CSS (LESS) Build
function cssBuild(store) {

	var
		files   = '$store/web/webroot/WEB-INF/_ui-src/addons/euronicscommonaddon/**/*.less'.replace('$store', store.id),
		find    = '/_ui-src/addons/euronicscommonaddon/',
		replace = '/_ui-src/';

	return gulp.src(files, {cwd: config.rootDir, base: config.rootDir})
		.pipe(cache('cssbuild' + store.id))
		.pipe(rename(function(path) {
			logPath(path, 'csb i');
			path.dirname = path.dirname.replace(find, replace);
			logPath(path, 'csb o');
		}))
		.pipe(gulp.dest(config.rootDir))

}
gulp.task('cssbuildb2b', ['addonsb2b'], function() { return cssBuild(config.stores.b2b); });
gulp.task('cssbuildb2c', ['addonsb2c'], function() { return cssBuild(config.stores.b2c); });
gulp.task('cssbuild', ['cssbuildb2b', 'cssbuildb2c']);


// Task – CSS (LESS) Dist
function cssDist(store) {

	var
		files   = '$store/web/webroot/WEB-INF/_ui-src/responsive/themes/**/style.less'.replace('$store', store.id),
		find    = /\/WEB-INF\/_ui-src\/([^\/]+)\/themes\/([^\/]+)\/less/,
		replace = '/_ui/$1/theme-$2/css';

	return gulp.src(files, {cwd: config.rootDir, base: config.rootDir})
		.pipe(cache('cssdist' + store.id))
		.pipe(less())
		.pipe(rename(function(path) {
			logPath(path, 'csd i');
			path.dirname = path.dirname.replace(find, replace);
			logPath(path, 'csd o');
		}))
		.pipe(gulp.dest(config.rootDir));

}
gulp.task('cssdistb2b', ['cssbuildb2b'], function() { return cssDist(config.stores.b2b); });
gulp.task('cssdistb2c', ['cssbuildb2c'], function() { return cssDist(config.stores.b2c); });
gulp.task('cssdist', ['cssdistb2b', 'cssdistb2c']);


// Task - Default (Apply Addons & Duplicate JS)
gulp.task('default', ['addons', 'jsbuild', 'jsdist', 'cssbuild', 'cssdist']);


// Task - Addons Watch
gulp.task('watch', ['default'], function() {

	var filesB2b = _.map(config.stores.b2b.addons, function(addon) {
		return '$addon/acceleratoraddon/web/webroot/**/*.*'.replace('$addon', addon);
	});

	var filesB2c = _.map(config.stores.b2c.addons, function(addon) {
		return '$addon/acceleratoraddon/web/webroot/**/*.*'.replace('$addon', addon);
	});
	
	gulp.watch(filesB2b, {cwd: config.rootDir}, ['addonsb2b', 'jsbuildb2b', 'jsdistb2b', 'cssbuildb2b', 'cssdistb2b']);
	gulp.watch(filesB2c, {cwd: config.rootDir}, ['addonsb2c', 'jsbuildb2c', 'jsdistb2c', 'cssbuildb2c', 'cssdistb2c']);

});


// Task - Serve (Browser Sync)
// gulp.task('serve', ['addons'], function() {

// 	browserSync.init({
// 		open: 'external',
// 		host: 'euronics.local',
// 		proxy: 'https://localhost:9002'
// 	});

// 	gulp.watch(['membersstorefront/**/*.*', 'euronicsstorefront/**/*.*'], {cwd: config.rootDir})
// 		.on('change', browserSync.reload);

// });


